Funções para serem implementadas no programa
============================================

Este arquivo ilustra o esqueleto de um programa a ser trabalhado por varios colaboradores. 
Trabalhar em lugares distintos de um mesmo arquivo evita conflitos na hora de reunir as funcionalidades (git merge).

Desta maneira, durante a execução de um projeto, após as atribuições de tarefas, cada aluno deve criar um branch para trabalhar. Ex: $ git branch conversorAD; git checkout conversorAD

Neste exercício você irá fazer suas modificações no projeto em um ramo exclusivo, 
enviar o ramo para o servidor do repositório e solicitar um merge das suas contribuições 
para o gerente do projeto (neste exercício o professor).
   
Passo-a-passo
-------------

1. Mover o diretório de trabalho (checkout) para uma versão do projeto no ramo master de interesse para criação do fork
2. Criar um novo ramo (branch)
3. Mover o diretório de trabalho para o ramo criado (checkout)
4. Fazer as suas modificações nos arquivos 
5. Salvar nova versão no repositório local (commit)
6. Enviar o novo ramo para o repositório (push)
7. Fazer uma requisição de merge ao gerente do projeto (ou realizar o merge com o master você mesmo)


Funções a serem implementadas
-----------------------------

Inclua algum texto na função atribuida a você.

__void FunçãoESHM(int a)__ 
tralalalala

__void FunçãoERF(int a)__
aloalo


__void FunçãoFSBR(int a)__
Teste do francio

__void FunçãoLS(int a)__
Alteração feita

__void FunçãoLCO(int a)__


__void FunçãoLSL(int a)__
FOGO NO BURACO!

__void FunçãoLPM(int a)__
<<<<<<< HEAD
<<<<<<< HEAD
Alteração feita na minha função.
=======
Modificação na minha função.
>>>>>>> LucasM
=======
Modificando a minha função...
>>>>>>> LucasMartins

__void FunçãoPHOF(int a)__
Olá mundo!

__void FunçãoRAD(int a)__
Let's see if this will work now...

__void FunçãoRAZR(int a)__
A BOMBA FOI PLANTADA

__void FunçãoSH(int a)__
Samuel Hammer: U CAN'T TOUCH THIS




